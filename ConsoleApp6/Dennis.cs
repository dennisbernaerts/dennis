﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp6
{
    static class Dennis
    {
        static public void FirstDays()
        {
            char letter = 'a';
            string name = "dennis";
            int dennisOuderdom = 46;
            int aantalZoons = 1;
            int wendyOuderdom = 43;
            int posInt = 2147483647;
            int negInt = -2147483648;
            uint unsignedInteger = (uint)2147483646*2; 

            bool trueFinished = true;
            bool falseFinished = false;

            double temperatuur = 0.6;
            float  floatTemperatuur = 0.6f;

            System.Int32 a = 100;
            System.String b = "vic";
            //int b = 100;



            System.Console.WriteLine("Hello World!");
            Console.WriteLine("dennis");

            Console.WriteLine('a' + "dennis" + 'a');
            Console.WriteLine("dennis is " + dennisOuderdom + " jaar en heeft " + aantalZoons + " zoons en zen vrouw is " + wendyOuderdom);
            Console.WriteLine($"dennis is {dennisOuderdom} jaar en heeft {aantalZoons} zoons en zen vrouw is {wendyOuderdom}");

            Console.WriteLine(string.Format("{0}", letter));
            Console.WriteLine("{0}{1}{0}", letter, name); // gebruik van array.
            Console.WriteLine("{1}{0}{2}", letter, name, letter);


            if (letter == 'a')
            {
                Console.WriteLine("letter is a");
            }
            else
            {
                Console.WriteLine("letter is niet a");
            }

            aantalZoons = 0;
            Console.WriteLine($"ik heb {(aantalZoons == 0 ? "geen" : "" + aantalZoons)} {(aantalZoons > 1 ? "zonen" : "zoon")}");

            aantalZoons = 1;
            Console.WriteLine($"ik heb {(aantalZoons == 0 ? "geen" : "" + aantalZoons)} {(aantalZoons > 1 ? "zonen" : "zoon")}");

            aantalZoons = 2;
            Console.WriteLine($"ik heb {(aantalZoons == 0 ? "geen" : "" + aantalZoons)} {(aantalZoons > 1 ? "zonen" : "zoon")}");

            ConsoleKeyInfo keyinfo = System.Console.ReadKey();

            //Op een console word je naam gevraagd en ACHTER de vraag kan je je naam intypen.
            //Op de 2de lijn komt een begroeting met je naam in eg "Dag Hilde, Hoe gaat het "
            //Als je dan de letter Q intypt stopt het programma.
            //Als je dan de letter R intypt vraagt hij je naam nogmaal, en begroet hij je nogmaals.


            string[] array = new string[5] { "m", "a", "n", "a", "l" };

            int x = 0;
            do
            {
                Console.WriteLine(x);

                Console.Write(array[x]);
                x = x + 1;
            } while (x < array.Length);

            for(x=1;x<=10;x++)
            {
                Console.WriteLine(x);
            }

            x = 1;
            while(x<10)
            {
                Console.WriteLine(x++);
            }


        }

    }
}


﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp6
{
    static class Koen
    {

        static void FristDays()
        {
            /*string naam = "naam";
            string letter = "a";
            Console.WriteLine("Hello World!");
            Console.WriteLine((char)'a');
            Console.WriteLine("{0}{1}{0}", letter, naam);

            int x = 5;
            x &= 1; //BITWISE and operator: x = x | 1
            Console.WriteLine($"|= operator, wth do you do : {x}");

            x = 5;
            x |= 3; //BITWISE or operator: x = x & 3
            Console.WriteLine($"&= operator : {x}");
            // 1 in bits == 0 0 0 0 0 0 0 1
            // 1 in bits == 0 0 0 0 0 0 1 0
            // 3 in bits == 0 0 0 0 0 0 1 1
            // 4 in bits == 0 0 0 0 0 1 0 0
            // 5 in bits == 0 0 0 0 0 1 0 1
            */

            //Lees naam in
            ReadWriteName();

            var keyStroke = Console.ReadKey(true).KeyChar;
            while (keyStroke != 'Q' && keyStroke != 'q') //KeyStroke kan ook naar capital gezet worden
            {
                if (keyStroke == 'R' || keyStroke == 'r')
                {
                    ReadWriteName();
                }
                keyStroke = Console.ReadKey(true).KeyChar;
            }


            Console.WriteLine("Hoeveel zonen heb je?");
                int aantalZonen = 0;
            Int32.TryParse(Console.ReadLine(), out aantalZonen);

                if (aantalZonen == 0)
                    Console.WriteLine("Je hebt geen zoon");
                else if (!(aantalZonen< 0))
                    Console.WriteLine($"Je hebt {aantalZonen} {((aantalZonen > 1) ? "Zonen" : "zoon")}");

                string zonenTekst = (aantalZonen < 0) ? "Je kan geen negatieven kinderen hebben" : (aantalZonen == 0) ? "Je hebt geen zonen" : (aantalZonen > 1) ? $"Je hebt {aantalZonen} zonen" : "Je hebt 1 zoon";
            Console.WriteLine(zonenTekst);
        }

        static void ReadWriteName()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Dag gebruiken, wat is jou naam? ");
            Console.ForegroundColor = ConsoleColor.White;
            string name = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine($"Dag {name}");
            Console.ForegroundColor = ConsoleColor.White;
        }

    }
}
